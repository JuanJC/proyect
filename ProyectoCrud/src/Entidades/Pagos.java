/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

import java.sql.Date;

/**
 *
 * @author Juan Carranza
 */
public class Pagos {
    Integer npagos;
    double ipagado;
    Date fecha;
    double monto;
    double saldo;
    Integer nprestamo;

    public Pagos() {
    }

    public Pagos(Integer npagos, double ipagado, Date fecha, double monto, double saldo, Integer nprestamo) {
        this.npagos = npagos;
        this.ipagado = ipagado;
        this.fecha = fecha;
        this.monto = monto;
        this.saldo = saldo;
        this.nprestamo = nprestamo;
    }

    public Integer getNpagos() {
        return npagos;
    }

    public void setNpagos(Integer npagos) {
        this.npagos = npagos;
    }

    public double getIpagado() {
        return ipagado;
    }

    public void setIpagado(double ipagado) {
        this.ipagado = ipagado;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public double getMonto() {
        return monto;
    }

    public void setMonto(double monto) {
        this.monto = monto;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    public Integer getNprestamo() {
        return nprestamo;
    }

    public void setNprestamo(Integer nprestamo) {
        this.nprestamo = nprestamo;
    }

    @Override
    public String toString() {
        return "Pagos{" + "npagos=" + npagos + ", ipagado=" + ipagado + ", fecha=" + fecha + ", monto=" + monto + ", saldo=" + saldo + ", nprestamo=" + nprestamo + '}';
    }
    
    
}
