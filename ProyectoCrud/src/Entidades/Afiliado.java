/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

import java.sql.Date;

/**
 *
 * @author Juan Carranza
 */
public class Afiliado {
    Integer codigo;
    String Pnom;
    String Snom;
    String Pape;
    String Sape;
    String dire;
    String dept;
    String ciudad;
    String calle;
    Integer casa;
    String ref;
    Date inicio;
    Date nacimiento;
    String pCorreo;
    String sCorreo;
    Integer Idgan;

    public Afiliado() {
    }

    public Afiliado(Integer codigo, String Pnom, String Snom, String Pape, String Sape, String dire, String dept, String ciudad, String calle, Integer casa, String ref, Date inicio, Date nacimiento, String pCorreo, String sCorreo, Integer Idgan) {
        this.codigo = codigo;
        this.Pnom = Pnom;
        this.Snom = Snom;
        this.Pape = Pape;
        this.Sape = Sape;
        this.dire = dire;
        this.dept = dept;
        this.ciudad = ciudad;
        this.calle = calle;
        this.casa = casa;
        this.ref = ref;
        this.inicio = inicio;
        this.nacimiento = nacimiento;
        this.pCorreo = pCorreo;
        this.sCorreo = sCorreo;
        this.Idgan = Idgan;
    }

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public String getPnom() {
        return Pnom;
    }

    public void setPnom(String Pnom) {
        this.Pnom = Pnom;
    }

    public String getSnom() {
        return Snom;
    }

    public void setSnom(String Snom) {
        this.Snom = Snom;
    }

    public String getPape() {
        return Pape;
    }

    public void setPape(String Pape) {
        this.Pape = Pape;
    }

    public String getSape() {
        return Sape;
    }

    public void setSape(String Sape) {
        this.Sape = Sape;
    }

    public String getDire() {
        return dire;
    }

    public void setDire(String dire) {
        this.dire = dire;
    }

    public String getDept() {
        return dept;
    }

    public void setDept(String dept) {
        this.dept = dept;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getCalle() {
        return calle;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    public Integer getCasa() {
        return casa;
    }

    public void setCasa(Integer casa) {
        this.casa = casa;
    }

    public String getRef() {
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }

    public Date getInicio() {
        return inicio;
    }

    public void setInicio(Date inicio) {
        this.inicio = inicio;
    }

    public Date getNacimiento() {
        return nacimiento;
    }

    public void setNacimiento(Date nacimiento) {
        this.nacimiento = nacimiento;
    }

    public String getpCorreo() {
        return pCorreo;
    }

    public void setpCorreo(String pCorreo) {
        this.pCorreo = pCorreo;
    }

    public String getsCorreo() {
        return sCorreo;
    }

    public void setsCorreo(String sCorreo) {
        this.sCorreo = sCorreo;
    }

    public Integer getIdgan() {
        return Idgan;
    }

    public void setIdgan(Integer Idgan) {
        this.Idgan = Idgan;
    }

    @Override
    public String toString() {
        return "Afiliado{" + "codigo=" + codigo + ", Pnom=" + Pnom + ", Snom=" + Snom + ", Pape=" + Pape + ", Sape=" + Sape + ", dire=" + dire + ", dept=" + dept + ", ciudad=" + ciudad + ", calle=" + calle + ", casa=" + casa + ", ref=" + ref + ", inicio=" + inicio + ", nacimiento=" + nacimiento + ", pCorreo=" + pCorreo + ", sCorreo=" + sCorreo + ", Idgan=" + Idgan + '}';
    }
    
    
}
