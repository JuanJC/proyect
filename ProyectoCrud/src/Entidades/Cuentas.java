/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

import java.sql.Date;

/**
 *
 * @author Juan Carranza
 */
public class Cuentas {
    Integer cuenta;
    Date apertura;
    String tipo;
    double saldo;
    Integer tiempo;
    Integer codigo;

    public Cuentas() {
    }

    public Cuentas(Integer cuenta, Date apertura, String tipo, double saldo, Integer tiempo, Integer codigo) {
        this.cuenta = cuenta;
        this.apertura = apertura;
        this.tipo = tipo;
        this.saldo = saldo;
        this.tiempo = tiempo;
        this.codigo = codigo;
    }

    public Integer getCuenta() {
        return cuenta;
    }

    public void setCuenta(Integer cuenta) {
        this.cuenta = cuenta;
    }

    public Date getApertura() {
        return apertura;
    }

    public void setApertura(Date apertura) {
        this.apertura = apertura;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    public Integer getTiempo() {
        return tiempo;
    }

    public void setTiempo(Integer tiempo) {
        this.tiempo = tiempo;
    }

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    @Override
    public String toString() {
        return "Cuentas{" + "cuenta=" + cuenta + ", apertura=" + apertura + ", tipo=" + tipo + ", saldo=" + saldo + ", tiempo=" + tiempo + ", codigo=" + codigo + '}';
    }
    
    
}
