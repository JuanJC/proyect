/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

/**
 *
 * @author Juan Carranza
 */
public class AfiliadosCuenta {
    Integer nprestamo;
    double tasa;
    Integer codigo;

    public AfiliadosCuenta(Integer nprestamo, double tasa, Integer codigo) {
        this.nprestamo = nprestamo;
        this.tasa = tasa;
        this.codigo = codigo;
    }

    public AfiliadosCuenta() {
    }

    public Integer getNprestamo() {
        return nprestamo;
    }

    public void setNprestamo(Integer nprestamo) {
        this.nprestamo = nprestamo;
    }

    public double getTasa() {
        return tasa;
    }

    public void setTasa(double tasa) {
        this.tasa = tasa;
    }

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    @Override
    public String toString() {
        return "AfiliadosCuenta{" + "nprestamo=" + nprestamo + ", tasa=" + tasa + ", codigo=" + codigo + '}';
    }
    
      
}
