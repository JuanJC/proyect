/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;



/**
 *
 * @author Juan Carranza
 */
public class Telefono {
    Integer codigo;
    Integer telefonos[] ;

    public Telefono(Integer codigo, Integer[] telefonos) {
        this.codigo = codigo;
        this.telefonos = telefonos;
    }

    public Telefono() {
    }

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public Integer[] getTelefonos() {
        return telefonos;
    }

    public void setTelefonos(Integer[] telefonos) {
        this.telefonos = telefonos;
    }
    
    
    
}
