/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

import java.sql.Date;

/**
 *
 * @author Juan Carranza
 */
public class Abono {
    Integer codigo;
    Date fecha;
    String des;
    double monto;
    double ret;
    Integer cuenta;

    public Abono() {
    }

    public Abono(Integer codigo, Date fecha, String des, double monto, double ret, Integer cuenta) {
        this.codigo = codigo;
        this.fecha = fecha;
        this.des = des;
        this.monto = monto;
        this.ret = ret;
        this.cuenta = cuenta;
    }

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getDes() {
        return des;
    }

    public void setDes(String des) {
        this.des = des;
    }

    public double getMonto() {
        return monto;
    }

    public void setMonto(double monto) {
        this.monto = monto;
    }

    public double getRet() {
        return ret;
    }

    public void setRet(double ret) {
        this.ret = ret;
    }

    public Integer getCuenta() {
        return cuenta;
    }

    public void setCuenta(Integer cuenta) {
        this.cuenta = cuenta;
    }

    @Override
    public String toString() {
        return "Abono{" + "codigo=" + codigo + ", fecha=" + fecha + ", des=" + des + ", monto=" + monto + ", ret=" + ret + ", cuenta=" + cuenta + '}';
    }
    
}
