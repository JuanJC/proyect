/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

import java.sql.Date;

/**
 *
 * @author Juan Carranza
 */
public class Prestamos {
    Integer nprestamo;
    Integer periodo;
    Date fecha;
    double monto;
    double saldo;

    public Prestamos(Integer nprestamo, Integer periodo, Date fecha, double monto, double saldo) {
        this.nprestamo = nprestamo;
        this.periodo = periodo;
        this.fecha = fecha;
        this.monto = monto;
        this.saldo = saldo;
    }

    public Prestamos() {
    }

    public Integer getNprestamo() {
        return nprestamo;
    }

    public void setNprestamo(Integer nprestamo) {
        this.nprestamo = nprestamo;
    }

    public Integer getPeriodo() {
        return periodo;
    }

    public void setPeriodo(Integer periodo) {
        this.periodo = periodo;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public double getMonto() {
        return monto;
    }

    public void setMonto(double monto) {
        this.monto = monto;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    @Override
    public String toString() {
        return "Prestamos{" + "nprestamo=" + nprestamo + ", periodo=" + periodo + ", fecha=" + fecha + ", monto=" + monto + ", saldo=" + saldo + '}';
    }
    
    
}
