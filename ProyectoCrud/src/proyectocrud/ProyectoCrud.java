/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectocrud;

/**
 *
 * @author Juan Carranza
 */
import java.sql.*;

public class ProyectoCrud {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        final String URL = "jdbc:postgresql://localhost:5432/Project";
        final String DRIVER= "org.postgresql.Driver";
        final String USUARIO = "postgres";
        final String CLAVE = "Vercetti1";
        
        Connection con = null;
        
        try{
           Class.forName(DRIVER); 
           try {
               con = DriverManager.getConnection(URL, USUARIO, CLAVE);
               String sql = "SELECT CODIGO, PRIMER_NOMBRE, SEGUNDO_NOMBRE, PRIMER_APELLIDO, SEGUNDO_APELLIDO, DEPARTAMENTO, CIUDAD, AVENIDA, CALLE, #CASA, REFERENCIA, FECHA_INICIO, FECHA_NACIMIENTO, PRIMER_CORREO, SEGUNDO_CORREO, ID_GANANCIAS\n" +
            "FROM public.AFILIADOS;"; 
           PreparedStatement pstm = con.prepareStatement(sql);
           ResultSet rst = pstm.executeQuery();
           while(rst.next() ){
               System.out.println(rst.getInt(1));
               System.out.println(rst.getString("PRIMER NOMBRE"));
               System.out.println(rst.getString("SEGUNDO NOMBRE"));
               System.out.println(rst.getString("PRIMER APELLIDO"));
               System.out.println(rst.getString("SEGUNDO APELLIDO"));
               System.out.println(rst.getString("DEPARTAMENTO"));
               System.out.println(rst.getString("CIUDAD"));
               System.out.println(rst.getString("AVENIDA"));
               System.out.println(rst.getString("#CALLE"));
               System.out.println(rst.getString("REFERENCIA"));
               System.out.println(rst.getString("FECHA INICIO"));
               System.out.println(rst.getString("FECHA NACIMIENTO")); 
               System.out.println(rst.getString("PRIMER CORREO"));
               System.out.println(rst.getString("SEGUNDO CORREO"));
               System.out.println(rst.getString("ID_GANANCIAS"));
           }
           } catch(SQLException e){
               System.out.println(e.getMessage());
           } finally {
               if(con != null){
                   try{
                       con.close(); 
                   }catch(Exception e){
                       System.out.println(e.getMessage());
                   }
               }
           }
           
        }catch(ClassNotFoundException e){
            System.out.println(e.getMessage());
        }
    }
     
}
