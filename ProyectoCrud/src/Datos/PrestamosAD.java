/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Datos;

import Conexion.Conectar;
import Entidades.Prestamos;
import java.sql.Connection;
import java.sql.PreparedStatement;

/**
 *
 * @author Juan Carranza
 */
public class PrestamosAD {
    Connection conexion;
    public void Insertar(Prestamos pr){
           PreparedStatement sentencia= null;
        Conectar con = new Conectar();
        con.Conector();
        conexion= con.getConnection();
        try{
            String sql = "SELECT FN_PRESTAMOS_INSERT('"+null+",'"+pr.getPeriodo()+"', '"+pr.getFecha()+"', '"+pr.getMonto()+"', '"+pr.getSaldo()+"');";
            sentencia = this.conexion.prepareStatement(sql);
        }catch(Exception e){
            System.err.print("Error");
        }
    }
    
    public void Update(Prestamos pr){
    PreparedStatement sentencia= null;
        Conectar con = new Conectar();
        con.Conector();
        conexion= con.getConnection();
        try{
            String sql = "SELECT FN_PRESTAMOS_UPDATE('"+pr.getPeriodo()+"', '"+pr.getFecha()+"', '"+pr.getMonto()+"', '"+pr.getSaldo()+"');";
            sentencia = this.conexion.prepareStatement(sql);
        }catch(Exception e){
            System.err.print("Error");
        }    
    }
    
    public void Delete(Integer codigo){
        PreparedStatement sentencia= null;
        Conectar con = new Conectar();
        con.Conector();
        conexion= con.getConnection();
        try{
            String sql = "SELECT FN_PRESTAMOS_DELETE('"+codigo+"');";
            sentencia = this.conexion.prepareStatement(sql);
        }catch(Exception e){
            System.err.print("Error");
        }
        
    }
    
    public void Lista(){
        PreparedStatement sentencia= null;
        Conectar con = new Conectar();
        con.Conector();
        conexion= con.getConnection();
        try{
            String sql = "SELECT FN_PRESTAMOS_READ();";
            sentencia = this.conexion.prepareStatement(sql);
        }catch(Exception e){
            System.err.print("Error");
        }
    }
}
