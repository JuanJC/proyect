/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Datos;

import Conexion.Conectar;
import Entidades.Abono;
import java.sql.Connection;
import java.sql.PreparedStatement;

/**
 *
 * @author Juan Carranza
 */
public class AbonoAD {
    Connection conexion;
    public void Insertar(Abono ab){
    PreparedStatement sentencia= null;
        Conectar con = new Conectar();
        con.Conector();
        conexion= con.getConnection();
        try{
            String sql = "SELECT FN_ABONOS_INSERT('"+null+"','"+ab.getCuenta()+"', '"+ab.getFecha()+"','"+ab.getMonto()+"','"+ab.getRet()+"');";
            sentencia = this.conexion.prepareStatement(sql);
        }catch(Exception e){
            System.err.print("Error");
        }
    }
    
    public void Update(Abono ab){
        PreparedStatement sentencia= null;
        Conectar con = new Conectar();
        con.Conector();
        conexion= con.getConnection();
        try{
            String sql = "SELECT FN_ABONOS_UPDATE'"+ab.getCuenta()+"', '"+ab.getFecha()+"','"+ab.getMonto()+"','"+ab.getRet()+"');";
            sentencia = this.conexion.prepareStatement(sql);
        }catch(Exception e){
            System.err.print("Error");
        }
    }
    
    public void Delete(Integer codigo){
        PreparedStatement sentencia= null;
        Conectar con = new Conectar();
        con.Conector();
        conexion= con.getConnection();
        try{
            String sql = "SELECT FN_ABONOS_DELETE('"+codigo+"');";
            sentencia = this.conexion.prepareStatement(sql);
        }catch(Exception e){
            System.err.print("Error");
        }
    }
    
    public void Leer(){
        PreparedStatement sentencia= null;
        Conectar con = new Conectar();
        con.Conector();
        conexion= con.getConnection();
        try{
            String sql = "SELECT FN_ABONOS_READ();";
            sentencia = this.conexion.prepareStatement(sql);
        }catch(Exception e){
            System.err.print("Error");
        }
    }
}
