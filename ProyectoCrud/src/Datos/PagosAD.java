/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Datos;

import Conexion.Conectar;
import Entidades.Pagos;
import java.sql.Connection;
import java.sql.PreparedStatement;

/**
 *
 * @author Juan Carranza
 */
public class PagosAD {
    Connection conexion;
    public void Insertar(Pagos p){
           PreparedStatement sentencia= null;
        Conectar con = new Conectar();
        con.Conector();
        conexion= con.getConnection();
        try{
            String sql = "SELECT FN_PAGOS_INSERT('"+null+"','"+p.getIpagado()+"', '"+p.getFecha()+"', '"+p.getMonto()+"', '"+p.getSaldo()+"','"+null+");";
            sentencia = this.conexion.prepareStatement(sql);
        }catch(Exception e){
            System.err.print("Error");
        }
    }
    
    public void Update(Pagos p){
           PreparedStatement sentencia= null;
        Conectar con = new Conectar();
        con.Conector();
        conexion= con.getConnection();
        try{
            String sql = "SELECT FN_PAGOS_UPDATE('"+p.getIpagado()+"', '"+p.getFecha()+"', '"+p.getMonto()+"', '"+p.getSaldo()+"');";
            sentencia = this.conexion.prepareStatement(sql);
        }catch(Exception e){
            System.err.print("Error");
        }    
    }
    
    public void Delete(Integer codigo){
           PreparedStatement sentencia= null;
        Conectar con = new Conectar();
        con.Conector();
        conexion= con.getConnection();
        try{
            String sql = "SELECT FN_PAGOS_DELETE('"+codigo+"');";
            sentencia = this.conexion.prepareStatement(sql);
        }catch(Exception e){
            System.err.print("Error");
        }
        
    }
    
    public void Leer(){
           PreparedStatement sentencia= null;
        Conectar con = new Conectar();
        con.Conector();
        conexion= con.getConnection();
        try{
            String sql = "SELECT FN_PAGOS_READ();";
            sentencia = this.conexion.prepareStatement(sql);
        }catch(Exception e){
            System.err.print("Error");
        }
        
    }
}
