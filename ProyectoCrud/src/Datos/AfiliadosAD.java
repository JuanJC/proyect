/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Datos;

import Conexion.Conectar;
import Entidades.Afiliado;
import java.sql.Connection;
import java.sql.PreparedStatement;

/**
 *
 * @author Juan Carranza
 */
public class AfiliadosAD {
    
Connection conexion;
    public void Insertar(Afiliado af){
        PreparedStatement sentencia= null;
        Conectar con = new Conectar();
        con.Conector();
        conexion= con.getConnection();
        try{
           String sql = "SELECT FN_AFILIADOS_INSERT('"+null+"'" +af.getPnom()+"','"+af.getSnom()+"','"+af.getPape()+"','"+af.getSape()+"','"+af.getDept()+"' ,'"+af.getCiudad()+"' ,'"+af.getDire()+"'" + ",'"+af.getCalle()+"' ,'"+af.getCasa()+"','"+af.getRef()+"','"+af.getInicio()+"' ,'"+af.getNacimiento()+"','"+af.getpCorreo()+"','"+af.getsCorreo()+"'"+null+");";
           sentencia = this.conexion.prepareStatement(sql);
           sentencia.executeUpdate();
        }
        catch(Exception e){
            
        }
    }
    
    public void Update(Afiliado af){
            PreparedStatement sentencia= null;
        Conectar con = new Conectar();
        con.Conector();
        conexion= con.getConnection();
        try{
           String sql =null;
                sql = "SELECT FN_AFILIADOS_UPDATE('" +af.getPnom()+"','"+af.getSnom()+"','"+af.getPape()+"','"+af.getSape()+"','"+af.getDept()+"' ,'"+af.getCiudad()+"' ,'"+af.getDire()+"'" + ",'"+af.getCalle()+"' ,'"+af.getCasa()+"','"+af.getRef()+"','"+af.getInicio()+"' ,'"+af.getNacimiento()+"','"+af.getpCorreo()+"','"+af.getsCorreo()+"'"+null+");";
           sentencia = this.conexion.prepareStatement(sql);
           sentencia.executeUpdate();
        }
        catch(Exception e){
            
        }
        
    }
    
    public void Delete(Integer codigo){
        PreparedStatement sentencia= null;
         Conectar con = new Conectar();
        con.Conector();
        conexion= con.getConnection();
        try{
           String borrar ="SELECT FN_AFILIADOS_DELETE("+codigo+"');"; 
        
           sentencia = this.conexion.prepareStatement(borrar);
           sentencia.executeUpdate();
        } catch(Exception e){
            
        }
    }
    
    public void Leer(){
        PreparedStatement sentencia= null;
        Conectar con = new Conectar();
        con.Conector();
        conexion= con.getConnection();
        try{
            String select ="SELECT FN_AFILIADOS_READ();";
         sentencia = this.conexion.prepareStatement(select);
           sentencia.executeUpdate();
        }
        catch(Exception e){
            
        }
    }
}
